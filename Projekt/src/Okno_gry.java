import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Okno_gry extends Plansza
{
	
	public JFrame okno1;
	public JLabel[][] pole;
	public JPanel plansza;
	public int []x;
	public int tura;		// gracz 1 to krzyzyk, gracz 2 to kolko
	public static int clientOrServer;			// czy gra wlacza sie jako client czy server, server to 0, client 1
	
	/*
	// Powinna by� klasa dziedzicz�ca po okno_gry, kt�ra b�dzie obs�ugiwa� klikniecia!
	void obsluga_klikniec(int x, int y)
	{
		pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
	}
	*/
	
	public void nic()
	{
		
	}
	
	
	public Okno_gry(int n)
	{
		super();			// wywo�anie konstruktora bez argumentowego z klasy nadrzednej
		
	
		clientOrServer = n;
		
		pole = new JLabel[10][10];
		
		for(int i=0; i<10; i++)
		{
		    for(int j=0; j<10; j++)
		    {	
	//	    	System.out.println(i + "  "  + j);
		        pole[i][j]=new JLabel();
		    }
		}
		x = new int[10];
		x[0] = 11;
		
		tura = 1;
		utworz_okno();
		utworz_plansze();
		
		 chat();		// tworzy przycisk chatu 
	}
	
	
	public void chat()
	{
		JButton wlaczChat = new JButton("W��cz chat");
		wlaczChat.setBounds(550, 560, 100, 40);
		wlaczChat.setVisible(true);
		wlaczChat.addActionListener(new klikniecie_wlaczChat());
		okno1.add(wlaczChat);
		
	}

	static class klikniecie_wlaczChat implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{

			
			if(clientOrServer == 0)			// gdy gracz jest serverem
			{
				System.out.println(clientOrServer);
				Runnable runnable = new DrugiWatek();		// tworzymy ogolnie watek
				Thread watek2 = new Thread(runnable);
				watek2.start();		// chat otwierany jest w nowym watku
			}
			
			else							// gdy gracz jest clientem
			{
				Runnable runnable = new PierwszyWatek();		// tworzymy ogolnie watek
				Thread watek1 = new Thread(runnable);
				watek1.start();		// chat otwierany jest w nowym watku
			}

			
		}
		
	}
	
	public void utworz_okno()
	{
		okno1 = new JFrame ("Gomoku PgK");
		okno1.setSize(900 ,800);
		okno1.setVisible(true);
		okno1.setLocation(400, 100);
		okno1.setResizable(false);
		okno1.setLayout(null);
		okno1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		

		
		JButton statystyki = new JButton("Ranking");
		statystyki.setBounds(650, 560, 100, 40);
		statystyki.setVisible(true);
		statystyki.addActionListener(new klikniecie_statystyki());

		
		
		okno1.add(statystyki);
		
		
		
		/*JLabel jl = new JLabel();
		jl.setIcon(new ImageIcon(".\\img\\nic.jpg"));
		okno1.add(jl);
		jl.setVisible(true);
		jl.setBounds(10, 10, 50, 50);
		
		jl.addMouseListener(new MouseAdapter() // obs�uga klikniecia w JLabel 
		{
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jl.setIcon(new ImageIcon(".\\img\\x.jpg"));
            }
		});
		jl.setCursor(new Cursor(Cursor.HAND_CURSOR));  // zmiana kursora na �apke po najechaniu na jl*/
				
	}
	static class klikniecie_statystyki implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{
			
			//
			// DO TESTOWANIA NA BAZY DANYCH
			//
			RaportDb r1 = new RaportDb();
			r1.getData();
			
			
			//POPRZEDNIE STATYSTYKI
			//DbConnect db = new DbConnect();
			//db.getData();
			
		}
		
	}
	
	public void utworz_plansze()
	{
		
		
		plansza = new JPanel();
		plansza.setBackground(Color.DARK_GRAY);
		plansza.setBounds(50, 50, 500, 500);
		//plansza.setLayout(new FlowLayout());	// ustawia obok siebie (mo�na dodac po ile w rzedzie)
		plansza.setLayout(null);
		okno1.add(plansza);
		plansza.setVisible(true);
		
		
		for(int i = 0 ; i < 10 ; i++)
		{
			for (int j = 0; j < 10 ; j++)
			{
				dodaj_pole(i, j);
			}
		}


		
	}
	
	public void dodaj_pole(int x, int y)
	{	
		int os_x = x*50;
		int os_y = y*50;
		plansza.add(pole[x][y]);
		pole[x][y].setBounds(os_x, os_y, 50	, 50);
		pole[x][y].setVisible(true);
		pole[x][y].setIcon(new ImageIcon(".\\img\\nic.jpg"));
		
		
		
		pole[x][y].addMouseListener(new MouseAdapter() // obs�uga klikniecia w JLabel  przenies do funkcji
		{
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) 
            {
            	//obsluga_klikniec();
            	
            	if( tab[x][y] == 'c')
            	{      
            		
            	}
            	if (tab[x][y] == 'p')
            	{
            		if(tura == 1)	// 
            		{
            			pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
            			System.out.println("Klikniecie " + x + " " + y);
            			tab[x][y] = 'c';
            			tura = 2;
            			kolko_czy_krzyzyk = 'c';
            			sprawdzenie_krzyzyk();

            		}
            		else
            		{
            			pole[x][y].setIcon(new ImageIcon(".\\img\\o.jpg"));
            			System.out.println("Klikniecie " + x + " " + y);
            			tab[x][y] = 'r';
            			tura = 1;
            			kolko_czy_krzyzyk = 'r';
            			sprawdzenie_krzyzyk();
            		}
            		pole[x][y].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            	}
            	//pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));

            }
		});
		pole[x][y].setCursor(new Cursor(Cursor.HAND_CURSOR));
		
	}
	
	
		

}
	