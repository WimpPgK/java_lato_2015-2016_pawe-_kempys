package OperacjeNaPlikach;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ZanimZacznieszGrac.Logowanie.Komunikat;





public class AktualizujPlansza 
{	
	private char[][] tab;
	private JFrame okno1;
	private JLabel[][] pole;
	private JPanel plansza;
	private int []x;
	private JButton next;		// przewija do nast�pnego pliku
	private JButton prev;		// przewija do poprzedniego pliku
	private JButton cofnij;
	private WczytajPlik wczytajPlik;
	private int numerPliku;
	Komunikat komunikat;
	Komunikat komunikat2;
	
	public AktualizujPlansza(char[][] tab2, WczytajPlik wczytajPlik)
	{
		this.tab = tab2;
		this.wczytajPlik = wczytajPlik;
		numerPliku = 0;
		komunikat = new Komunikat();
		komunikat2 = new Komunikat();
		pole = new JLabel[10][10];
		
		for(int i=0; i<10; i++)
		{
		    for(int j=0; j<10; j++)
		    {	
			        pole[i][j]=new JLabel();
		    }
		}
		x = new int[10];
		x[0] = 11;
		

		utworz_okno();
		utworz_plansze();
		aktualizuj_plansze();
		
		
		
	}
	
	



		
		public void utworz_okno()
		{
			okno1 = new JFrame ("Gomoku PgK");
			okno1.setSize(600 ,650);
			okno1.setVisible(true);
			okno1.setLocation(400, 100);
			okno1.setResizable(false);
			okno1.setLayout(null);
			okno1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.out.println(x[0]);
			
			
			
			prev = new JButton ("Poprzedni");
			okno1.add(prev);
			prev.setVisible(true);
			prev.setBounds(50, 520, 250, 40);
			
			
			next = new JButton ("Nast�pny");
			okno1.add(next);
			next.setVisible(true);
			next.setBounds(300, 520, 250, 40);
			
			cofnij = new JButton ("Wyjscie do menu");
			okno1.add(cofnij);
			cofnij.setVisible(true);
			cofnij.setBounds(50, 560, 500, 40);
			
			prev.addActionListener(new klikniecie_prev());
			next.addActionListener(new klikniecie_next());
			cofnij.addActionListener(new klikniecie_cofnij());

					
		}
		
		
		
		class klikniecie_next implements ActionListener  
		{	
			
			public void actionPerformed (ActionEvent e)
			{


				try 
				{
					numerPliku ++;
					wczytajPlik.odczyt_z_pliku(numerPliku);
					System.out.println(numerPliku);
					aktualizuj_plansze();

				} 
				catch (Exception e1) 
				{
					komunikat.wypisz("Brak kolejnych rekord�w!");
					numerPliku --;
				}
				
				
			}
		}
		
		
		
		class klikniecie_prev implements ActionListener  
		{
			public void actionPerformed (ActionEvent e)
			{
				
	
				
				try 
				{
					numerPliku --;
					wczytajPlik.odczyt_z_pliku(numerPliku);
					System.out.println(numerPliku);
					aktualizuj_plansze();
					
				} 
				catch (Exception e1) 
				{
					komunikat2.wypisz("Brak wcze�niejszych rekord�w!");
					numerPliku ++;
				}
				
				

				
			}
		}
		
		
		
		class klikniecie_cofnij implements ActionListener  
		{
			public void actionPerformed (ActionEvent e)
			{
				
				
				okno1.setVisible(false);

				
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		public void utworz_plansze()
		{
			
			
			plansza = new JPanel();
			plansza.setBackground(Color.DARK_GRAY);
			plansza.setBounds(50, 10, 500, 500);
			//plansza.setLayout(new FlowLayout());	// ustawia obok siebie (mo�na dodac po ile w rzedzie)
			plansza.setLayout(null);
			okno1.add(plansza);
			plansza.setVisible(true);
			
			
			for(int i = 0 ; i < 10 ; i++)
			{
				for (int j = 0; j < 10 ; j++)
				{
					dodaj_pole(i, j);
				}
			}


			
		}
		
		public void dodaj_pole(int x, int y)
		{	
			int os_x = x*50;
			int os_y = y*50;
			plansza.add(pole[x][y]);
			pole[x][y].setBounds(os_x, os_y, 50	, 50);
			pole[x][y].setVisible(true);
			pole[x][y].setIcon(new ImageIcon(".\\img\\nic.jpg"));
			

		}
		
		void aktualizuj_plansze()
		{
			for(int x = 0 ; x < 10 ; x++)
			{
				for (int y = 0; y < 10 ; y++)
				{
					if (tab[x][y] == 'c')
					{
						pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
					}
					else if(tab[x][y] == 'r')
					{
						pole[x][y].setIcon(new ImageIcon(".\\img\\o.jpg"));
					}
					else
					{
						pole[x][y].setIcon(new ImageIcon(".\\img\\nic.jpg"));
					}
				}
			}

		}
		
		
		
	
}
