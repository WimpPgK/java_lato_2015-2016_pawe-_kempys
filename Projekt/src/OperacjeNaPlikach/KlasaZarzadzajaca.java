package OperacjeNaPlikach;

import ZanimZacznieszGrac.Logowanie.Komunikat;

public class KlasaZarzadzajaca 
{
	private AktualizujPlansza aktualizujPlansza;
	private planszaLogika plansza_logika;
	private WczytajPlik wczytajPlik;
	Komunikat komunikat;
	
	
	
	
	
	public KlasaZarzadzajaca()
	{	
		komunikat = new Komunikat();

		
		
		try 
		{
			plansza_logika = new planszaLogika();
			wczytajPlik = new WczytajPlik(plansza_logika);
			wczytajPlik.odczyt_z_pliku(0);
			wczytajPlik.wypisz();
			aktualizujPlansza = new AktualizujPlansza(plansza_logika.tab, wczytajPlik);
		} 
		catch (Exception e) 
		{
			komunikat.wypisz("Brak zapisanych stan�w gry");
		}

		
	}
	
	
}
