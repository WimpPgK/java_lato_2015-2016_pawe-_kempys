package OtwieraniePlanszy;


import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Client
{
	

	private ObjectOutputStream output;	// od clienta do servera
	private ObjectInputStream input;	// od servera do clienta
	private String message = "";
	private String serverIP;	// jako argument wejsciowy w konstruktorze
	private Socket connection;
	private Okno_gry okno_gry;
	private String[] kliknieciePrzeciwnika;
	
	
	public Client (String host, Okno_gry okno_gry)		// ip servera
	{	
		
		host = host.replace('/', ' ');
		host = host.trim();
		serverIP = host;
		this.okno_gry = okno_gry;
		okno_gry.komunikat01.setText("Po��czenie aktywne!");
		
		
		ChatGui();
		/* do nas�uchiwania tekstu i wysy�ania
		userText.addActionListener
			(
				new ActionListener()
				{
					public void actionPerformed (ActionEvent event)	// jak ktos walnei enter to sie to wykona
					{
						sendMessage(event.getActionCommand());		// wysy�a to co wpisalismy do userTexy
						userText.setText("");		// po wyslaniu wiadomosci pole tekstowe sie czysci
					}
				}
			);
		*/
;
	}
	
	
	//******************************  *********************************************
	//laczenie z serverem
	public void startRunning()
	{
		try
		{
			connectToServer();
			setupStreams();			//  tworzy strumienie
			whileChatting();		// przesylanie do siebie wiadomosci
		}
		catch(EOFException eofException)
		{
			System.out.println("\n Client terminated connection") ;
			
		}
		catch (IOException ioException) 	//IOException jest klas� podstawow� dla wyj�tki generowane podczas uzyskiwania dost�pu do informacji za pomoc� strumieni, plik�w i katalog�w.
		{
				ioException.printStackTrace();			
		}
		finally
		{
			closeCrap();
		}
	}
	
	
	//******************************  *********************************************
	//laczenie z serverem
	
	public void connectToServer() throws IOException
	{
		System.out.println("Attempting connection...\n");
		connection = new Socket (InetAddress.getByName(serverIP), 6798);
		System.out.println("Connected to: " + connection.getInetAddress().getHostName());
	}
	
	//******************************  *********************************************
	//ustawienie strumieni do wysylania i odbierania wiadomosci
	
	private void setupStreams() throws IOException
	{
		output = new ObjectOutputStream(connection.getOutputStream());		// nazwa socketu.getOu..
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		System.out.println("\n Your streams are now good to go");
		
	}
	
	//******************************  *********************************************
	// while chatting with server
	private void whileChatting() throws IOException
	{
		int x, y;
		do
		{
			try
			{
				// cokolwiek zostanie do nas przyslane to zostanie to zamienione na string
				message = (String) input.readObject();		// input - nazwa strumienia
				
				
				
						
				
				
				
				
				
				
				if (message.matches(".*\\ ATBf1 \\b.*"))	// jesli tekst to ruch gracza to go nie wyswietla w chacie
				{
					
					okno_gry.flagaTura = 1;
					kliknieciePrzeciwnika = message.split(" ");
					
					System.out.println(kliknieciePrzeciwnika[3]);			// 3 -  wspolrzedna x, 4 wspolrzedna y
					
					
					
					x = Integer.parseInt(kliknieciePrzeciwnika[3]);
					y = Integer.parseInt(kliknieciePrzeciwnika[4]);
					
					okno_gry.pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
					okno_gry.tab[x][y] = 'c';
					okno_gry.pole[x][y].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
					//okno_gry.sprawdzenie_krzyzyk();

					
					
					
				}
				else
				{
					appendText(message);
				}
				
				
				
				
				
					
				
				
				
				
				

				System.out.println ("\n" + message);
				
			}
			catch(ClassNotFoundException classNotFoundException)	// gdy dostaniemy cos co sie nie da wlazyc do stringa
			{
				System.out.println("\n I don't know that object type ");
			}
		}
		while(!message.equals("SERVER - END"));		// dopoki na chacie nie napiszesz END to chat bedzie dzialac
		
	}
	
	//******************************  *********************************************
	// zamykanie strumieni i socketow
	private void closeCrap()
	{
		System.out.println ("\n closing crap down...");
		
		try
		{
			output.close();
			input.close();
			connection.close(); 	// zamykanie gniazda
		}
		catch(IOException ioException)
		{
			ioException.printStackTrace();
		}
	}
	
	//******************************  *********************************************
	//wysylanie wiadomosci do servera
	public void sendMessage (String message)
	{
		try
		{
			output.writeObject("CLIENT - " + message);
			output.flush();
			System.out.println("\nCLIENT - " + message);
		}
		catch(IOException ioException)//	 56 1.40 tu jest jakis blad
		{
			System.out.println("\n something messed up sending message!");
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//********************************************************
	
	
	private JFrame jFrame;
	public JPanel jPanel;
	private JTextArea historiaChatu;
	private JTextField poleChatu;
	
	void ChatGui()
	{


		
		jPanel = new JPanel();
		jPanel.setBounds(0, 50, 400, 600);
		//jPanel.setBackground(Color.BLACK);
		jPanel.setLayout(null);

		
		dodajPolaTekstowe();
		
		poleChatu.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)		// jak ktos walnei enter to sie to wykona
				{	
					String WiadomoscDoWyslania = event.getActionCommand();
					sendMessage(WiadomoscDoWyslania);		// wysy�a to co wpisalismy do userTexy
					appendText("Ja: " + WiadomoscDoWyslania);
					poleChatu.setText("");		// po wyslaniu wiadomosci pole tekstowe sie czysci
				}

			}
		);
		
		
	}
	
	//dodaje pole historii chatu i pole do pisania
	void dodajPolaTekstowe()
	{
		historiaChatu = new JTextArea();		
		historiaChatu.setVisible(true);
		jPanel.add(historiaChatu);
		historiaChatu.setBounds(0,1, 300, 300);
		historiaChatu.setEditable(false);

		
		poleChatu = new JTextField();
		poleChatu.setVisible(true);
		jPanel.add(poleChatu);
		poleChatu.setBounds (0,303, 300, 30);
		
		
		
	}
	
	//dodaje tekst ktory zostal przeslany przez chat
	void appendText(String tekst)
	{
		historiaChatu.append("Wiadomosc: " + tekst + "\n");
	}
	
	
	
	
	//***************************************************************
	
	
	
	
	
	
	
	
}
