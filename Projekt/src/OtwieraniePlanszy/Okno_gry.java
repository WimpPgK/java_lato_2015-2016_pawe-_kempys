package OtwieraniePlanszy;



import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ZanimZacznieszGrac.Logowanie.LogowanieGui;



public class Okno_gry extends Plansza
{
	
	public JFrame okno1;
	public JLabel[][] pole;
	public JPanel plansza;
	public int []x;
	public int tura;		// gracz 1 to krzyzyk, gracz 2 to kolko
	public static int clientOrServer;			// czy gra wlacza sie jako client czy server, server to 0, client 1
	private Client client;
	private Server server;
	public int flagaTura;		// okre�la czy na planszy da si� klika� czy nie
	public String IPserver;
	private LogowanieGui logowanieGui;
	private int idStolu;
	JLabel komunikat01;
	JLabel komunikatTura;
	
	//potem usun
	
	
	
	/*
	// Powinna by� klasa dziedzicz�ca po okno_gry, kt�ra b�dzie obs�ugiwa� klikniecia!
	void obsluga_klikniec(int x, int y)
	{
		pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
	}
	*/
	
	public void nic()
	{
		
	}
	
	
	public Okno_gry(int n, String IPserver, LogowanieGui logowanieGui, int idStolu)
	{
		super();			// wywo�anie konstruktora bez argumentowego z klasy nadrzednej
		
		this.IPserver = IPserver;
		this.logowanieGui = logowanieGui;
		this.idStolu = idStolu; 	// zeby bylo wiadomo przy jakim stole siedzimy
		
		clientOrServer = n;
		flagaTura = 0;		// zaczyna server
		
		
		
		
		pole = new JLabel[10][10];
		
		for(int i=0; i<10; i++)
		{
		    for(int j=0; j<10; j++)
		    {	
		        pole[i][j]=new JLabel();
		    }
		}
		x = new int[10];
		x[0] = 11;
		
		tura = n + 1;
		utworz_okno();
		utworz_plansze();
		
		
		/* panel chatu   
		 * poczatek*/
		
		JPanel panelChatu = new JPanel();
		
		
		if(clientOrServer == 0)			// gdy gracz jest serverem
		{
			
			
			server = new Server(this);
			panelChatu = server.jPanel;
			panelChatu.setLocation(550, 100);
			okno1.add(panelChatu);
			
			
			
			class Watek implements Runnable		// implementacja interfejsu
			{
					
					public int iii;
					public void run() {
						try {
							TimeUnit.MILLISECONDS.sleep(500);		// watek uruchamia sie z opuznieneim
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("Jestem nowym watkeim - server");
						server.startRunning();
						
						
					
				}
			}
			Runnable runnable = new Watek();		// tworzymy ogolnie watek
			Thread watek = new Thread(runnable);
			watek.start();		// chat otwierany jest w nowym watku

			
			
			
			
		}
		
		else							// gdy gracz jest clientem
		{
			client = new Client (IPserver, this);
			panelChatu = client.jPanel;
			panelChatu.setLocation(550, 100);
			okno1.add(panelChatu);
			

			
			class Watek implements Runnable		// implementacja interfejsu
			{
					
					
					public void run() {
						try {
							TimeUnit.MILLISECONDS.sleep(500);		// watek uruchamia sie z opuznieneim
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						System.out.println("Jestem nowym watkeim - client");
						client.startRunning();
						
						
					
				}
			}
			Runnable runnable = new Watek();		// tworzymy ogolnie watek
			Thread watek = new Thread(runnable);
			watek.start();		// chat otwierany jest w nowym watku
			
		}
		 
	}
	
	

	
	
	public void utworz_okno()
	{
		okno1 = new JFrame ("Gomoku PgK");
		okno1.setSize(900 ,550);
		okno1.setVisible(true);
		okno1.setLocation(400, 100);
		okno1.setResizable(false);
		okno1.setLayout(null);
		okno1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		

		
		JButton statystyki = new JButton("Ranking");
		statystyki.setBounds(650, 450, 100, 40);
		statystyki.setVisible(true);
		statystyki.addActionListener(new klikniecie_statystyki());

		
		
		okno1.add(statystyki);
		
		
		
		komunikat01 = new JLabel("");
		komunikat01.setBounds(620, 35, 300, 40);
		komunikat01.setVisible(true);
		
		Font font = new Font("Verdana", Font.BOLD, 20);
		komunikat01.setFont(font);
		okno1.add(komunikat01);

		
		
		komunikatTura = new JLabel("dddddddddd");
		komunikatTura.setBounds(620, 50, 300, 100);
		komunikatTura.setVisible(false);

		
		Font font2 = new Font("Verdana", Font.BOLD, 30);
		komunikatTura.setFont(font2);
		okno1.add(komunikatTura);
		
		
		
		
		/*JLabel jl = new JLabel();
		jl.setIcon(new ImageIcon(".\\img\\nic.jpg"));
		okno1.add(jl);
		jl.setVisible(true);
		jl.setBounds(10, 10, 50, 50);
		
		jl.addMouseListener(new MouseAdapter() // obs�uga klikniecia w JLabel 
		{
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jl.setIcon(new ImageIcon(".\\img\\x.jpg"));
            }
		});
		jl.setCursor(new Cursor(Cursor.HAND_CURSOR));  // zmiana kursora na �apke po najechaniu na jl*/
				
	}
	static class klikniecie_statystyki implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{
			
			//
			// DO TESTOWANIA NA BAZY DANYCH
			//
			//RaportDb r1 = new RaportDb();
			//r1.getData();
			
			
			//POPRZEDNIE STATYSTYKI
			//DbConnect db = new DbConnect();
			//db.getData();
			
		}
		
	}
	
	public void utworz_plansze()
	{
		
		
		plansza = new JPanel();
		plansza.setBackground(Color.DARK_GRAY);
		plansza.setBounds(10, 10, 500, 500);
		//plansza.setLayout(new FlowLayout());	// ustawia obok siebie (mo�na dodac po ile w rzedzie)
		plansza.setLayout(null);
		okno1.add(plansza);
		plansza.setVisible(true);
		
		
		for(int i = 0 ; i < 10 ; i++)
		{
			for (int j = 0; j < 10 ; j++)
			{
				dodaj_pole(i, j);
			}
		}


		
	}
	
	void zmienTure()
	{
		if (flagaTura == clientOrServer)
		{
			komunikatTura.setText("Tw�j ruch!");
		}
		else
		{
			komunikatTura.setText("Ruch przeciwnika");
		}
	}
	
	public void dodaj_pole(int x, int y)
	{	
		int os_x = x*50;
		int os_y = y*50;
		plansza.add(pole[x][y]);
		pole[x][y].setBounds(os_x, os_y, 50	, 50);
		pole[x][y].setVisible(true);
		pole[x][y].setIcon(new ImageIcon(".\\img\\nic.jpg"));
		
		

		
		
		class Watek implements Runnable		// implementacja interfejsu
		{
			


		
				
				public void run() {
					try {
						TimeUnit.SECONDS.sleep(1);		// watek uruchamia sie z opuznieneim
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					

					
					
					pole[x][y].addMouseListener(new MouseAdapter() // obs�uga klikniecia w JLabel  przenies do funkcji
							{
					            @Override

					            public void mouseClicked(java.awt.event.MouseEvent evt) 
					            {
					            	
						            if ((flagaTura == clientOrServer) && (komunikat01.getText().equals("Po��czenie aktywne!")))	// czyli gdy tura gracza jest tura servera (0) clienta (1)
						            {
						            	//obsluga_klikniec();
	
						            	//komunikatTura.setText("Tw�j ruch!");
						            	
						            	if( tab[x][y] == 'c')
						            	{      
						            		
						            	}
						            	if (tab[x][y] == 'p')
						            	{
						            		if(tura == 1)	// 
						            		{
						            			pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
						            			System.out.println("Klikniecie " + x + " " + y);
						            		
						            			
						            			
						            			
						            			
						            			
						                		if(clientOrServer == 0)			// gdy gracz jest serverem
						                		{
						                			server.sendMessage("ATBf1 " + x + " " + y);
						                			flagaTura = 1;
						                		
				
						                		}
						                		else
						                		{
						                			client.sendMessage("ATBf1 " + x + " " + y);
						                			flagaTura = 0;
						                			
						                		}
						            			
						            			
						            			
						            			
						            			
						            			tab[x][y] = 'c';
						            			//tura = 2;
						            			kolko_czy_krzyzyk = 'c';
						            			sprawdzenie_krzyzyk();
	
						            		}
						            		else
						            		{
						            			pole[x][y].setIcon(new ImageIcon(".\\img\\o.jpg"));
						            			System.out.println("Klikniecie " + x + " " + y);
						            			
						            			
						                		if(clientOrServer == 0)			// gdy gracz jest serverem
						                		{
						                			server.sendMessage("ATBf1 " + x + " " + y);
						                			flagaTura = 1;
						                			
						            				
						                		}
						                		else
						                		{
						                			client.sendMessage("ATBf1 " + x + " " + y);
						                			flagaTura = 0;
						                			
						                		}
						            			
						            			
						            			
						            			
						            			
						            			tab[x][y] = 'r';
						            			//tura = 1;
						            			kolko_czy_krzyzyk = 'r';
						            			sprawdzenie_krzyzyk();
						            		}
						            		pole[x][y].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						            	}
					            	//pole[x][y].setIcon(new ImageIcon(".\\img\\x.jpg"));
						            }
						         
				
						        
					            }
							});
							pole[x][y].setCursor(new Cursor(Cursor.HAND_CURSOR));
					
					
					
					
				
			}
		}
		Runnable runnable = new Watek();		// tworzymy ogolnie watek
		Thread watek = new Thread(runnable);
		watek.start();		// chat otwierany jest w nowym watku
		
		
		
		
		
	}
	
	
		

}
	