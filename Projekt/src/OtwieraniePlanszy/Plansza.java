package OtwieraniePlanszy;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Plansza
{	
	private int n;
	protected char kolko_czy_krzyzyk;
	char [][] tab;
	
	public void sprawdzenie_krzyzyk()
	{
		sprawdz_krzywa01();
		sprawdz_krzywa02();
		sprawdz_poziom();
	}
	
	
	
	public void sprawdz_krzywa02()
	{
		//od lewej do prawej w dol
		for (int i = 0; i < n - 4; i++) // przechodzi w  pionie tak, zeby gdy natrafi na krzyzyk jest jeszcze miejsce gdzie mozna wygrac
											 // przechodzi o wiersz nizej
		{
			for (int j = 4; j < n; j++)	// przechodzi w poziomie, tez nie dochodzi do konca
			{
				int k = 0;
				for (k = 0; k < 5 && tab[i + k][j - k] == kolko_czy_krzyzyk; k++);	// gdy zgadza sie symbol, sprawdza, czy jest PRZYNAJMNIEJ 5 razy
				if (k == 5)												// napraw bo moze byc np. 6
				{
					okno_wygrana();
					//System.out.println("Wygrywa krzyzyk po skosie! 02");
				}
			}
		}

	}
	
	private void okno_wygrana()
	{
		JFrame okno = new JFrame ("Gomoku PgK");
		okno.setSize(300 ,200);
		okno.setVisible(true);
		okno.setLocation(600, 200);
		okno.setResizable(false);
		okno.setLayout(null);
		okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		// zamyka po zamknieciu wszystkie pozosra�e okna
		okno.setAlwaysOnTop(true);
		
		//************ napis *****************
		JLabel napis;
		if (kolko_czy_krzyzyk == 'c')
		{
			napis = new JLabel("Wygrywa krzy�yk!");
		}
		else
		{
			napis = new JLabel("Wygrywa k�ko!");
		}
		
		napis.setBounds(90,40,180,30);
		napis.setVisible(true);
		okno.add(napis);
		
		//************* przyciski ******************
		
		JButton przycisk01 = new JButton("Wyj�cie");
		JButton przycisk02 = new JButton("Nowa gra");
		przycisk01.setBounds(50, 80, 100, 30);
		przycisk02.setBounds(150, 80, 100, 30);
		
		przycisk01.setVisible(true);
		przycisk02.setVisible(true);
		
		okno.add(przycisk01);
		okno.add(przycisk02);
				
	
		
		
	}
	
	public void sprawdz_krzywa01()
	{
		//od prawej do lewej w dol
		for (int i = 0; i < n - 4; i++) // przechodzi w  pionie tak, zeby gdy natrafi na krzyzyk jest jeszcze miejsce gdzie mozna wygrac
											// przechodzi o wiersz nizej
		{
			for (int j = 0; j < n - 4; j++)	// przechodzi w poziomie, tez nie dochodzi do konca
			{
				int k = 0;
				for (k = 0; k < 5 && tab[i + k][j + k] == kolko_czy_krzyzyk; k++);	// gdy zgadza sie symbol, sprawdza, czy jest PRZYNAJMNIEJ 5 razy
				if (k == 5)												// napraw bo moze byc np. 6
				{
					okno_wygrana();
					//System.out.println("Wygrywa krzyzyk po skosie! 01");
				}
			}
		}

	}

	public void sprawdz_poziom()
	{	
				
		// sprawdza, czy nie ma wygranej w poziomie
		int i = 0;
		for (i = 0; i < n; i++)
		{
			for (int s = 0; s < n - 5 + 1; s++)
			{
				int j;
				for (j = 0; j < 5 && tab[i][j + s] == kolko_czy_krzyzyk; j++);	// trzeba by tu dac, zeby odrzucalo te co sie zgadza np. 6 pol
				if (j == 5)
				{
					okno_wygrana();
					//System.out.println("Wygrywa krzyzyk w poziomie!");
				}
			}
		}

		// sprawdza, czy nie ma wygranej w pionie
		int j = 0;
		
		for (j = 0; j < n; j++)
		{
			for (int s = 0; s < n - 5 + 1; s++)
			{
				for (i = 0; i < 5 && tab[i + s][j] == kolko_czy_krzyzyk; i++);	// trzeba by tu dac, zeby odrzucalo te co sie zgadza np. 6 pol
				if (i == 5)
				{
					okno_wygrana();
					//System.out.println("Wygrywa krzyzyk w pionie!");
				}
			}
		}

		// sprawdza po skosie od lewej do prawej w dol

	}
	
	
	
	
	
	
	
	
	
	
//****************************************************************************	
	Plansza()	// konstruktor
	{
		n = 10;
		tab = new char [10][10];
		
			for (int i = 0 ; i < n ; i++)
			{
				for (int j = 0 ; j < n ; j++)
				{
					tab[i][j] = 'p';
				}
			}


	}
	
//****************************************************************************	
	public void wypisz()
	{		
		for (int i = 0 ; i < n ; i++)
		{
			for (int j = 0 ; j < n ; j++)
			{
				System.out.print(tab[i][j]);
			}
			System.out.print("\n");
		}
	}
	
//****************************************************************************	
	public void odczyt_z_pliku()
	{
			String lokalizajcaPliku = "plik.txt";
			String linia = "";
				File plik = new File (lokalizajcaPliku);
				try
				{
					FileReader odczyt = new FileReader(plik);
					BufferedReader buforowanyOdczyt = new BufferedReader(odczyt);
							

					int i = 0;

					//************************************************************
					try
					{	
						for (i = 0 ; i < n ; i++)
						{
							linia = buforowanyOdczyt.readLine();
							for(int j = 0 ; j < n ; j++)
							{
								tab[i][j] = linia.charAt(j);
							}
						}

					}			
					catch(IOException e)
					{
						System.out.println("Wyjatek1" );
					}
					
					
					//*************************************************************
					try 
					{
						odczyt.close();
					} 
					catch (IOException e) 
					{
						System.out.println("Wyjatek1" );
						e.printStackTrace();
					}
					//***************************************************************
					
					
				} 
				catch (FileNotFoundException e)
				{
					
					e.printStackTrace();
				}
			}
	}


