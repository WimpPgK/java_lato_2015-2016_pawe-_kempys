package OtwieraniePlanszy;



import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.*;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import ZanimZacznieszGrac.Logowanie.Komunikat;
import ZanimZacznieszGrac.Logowanie.LogowanieGui;




public class Server 
{

	private ObjectOutputStream output;		// w pracy na plikach zapisywalo sie w tym obiekcie sciezke do pliku
	//output - strumien z mojego komputera do 2 komputera
	
	private ObjectInputStream input; // strumien do mojego komputera z komputera nr.2
	private ServerSocket server;	 // obiekt do obs�ugi po��czen, z rownoczesna obs�uga wielu polaczen
	private Socket connection;  	 // gniazdo polaczenia  (ustalic sockety to w tutorialu ustawic polaczenie)
	private Okno_gry okno_gry;
	private String [] kliknieciePrzeciwnika;	// przechowuje wspolrzedne klikniecia przeciwnika
	//private ChatGui gui;
	

	//constructor
	public Server(Okno_gry okno_gry)
	{	

		this.okno_gry = okno_gry;
		ChatGui();
	
	
	}
	
	
	//******************************  *********************************************
	//ustawia i uruchamia server
	
	public void startRunning()
	{
		try
		{
			server = new ServerSocket(6798,100);  // (numer portu(gdzie aplikacja jest na servere), ilosc ludzi mogacych czekac na port)
			//jesli wirtualna maszyna nie bedzie mogla zarezerwowac wybranego portu to wyrzuci wyjatek
			while (true)
			{
				try
				{
					// podlaczenie i przeprowadzanie rozmowy
					
					
					waitForConnection();  	// oczekiwanie na polaczenie

					
					
					
				}
				catch(EOFException eofException)	// wyjatek do oznaczania konca polaczenia (end of a stream)
				{
					System.out.println("\n Polaczenie zostalo zerwane! ");
				}
			}

		}
		catch (IOException ioException) //IOExceptionjest klas� podstawow� dla wyj�tki generowane podczas uzyskiwania dost�pu do informacji za pomoc� strumieni, plik�w i katalog�w.
		{
				ioException.printStackTrace();			
		}
	}
	
	
	
	
	//******************************  *********************************************
	//czeka na polaczenie, potem wyswietla informacje o polaczeniu
	
	private void waitForConnection() throws IOException		// deklaracja, ze ta funkcja moze wyrzucic wyjatek typu IOException
	{
		System.out.println("Waiting for someone connect... \n");
		
		//tworzenie polaczenia 
		while(true)
		{
			
			okno_gry.komunikat01.setText("OCZEKIWANIE...");

			
			connection = server.accept();
			
			
			new ServerThread(connection).start();	// wywo�anie start powoduje uruchomienei metody run
			System.out.println("OU YEAH! Odpalono nowy watek. ");
		
		}
			
	
	}
	
	
	
	
	
	
	
	
	
	
	
	
	//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
	
	
	public class ServerThread extends Thread 
	{
		Socket socket;
		
		ServerThread(Socket socket)
		{
			this.socket = socket;
		}
		
		public void run()
		{
			try {
				okno_gry.komunikat01.setText("Po��czenie aktywne!");
				setupStreams();
				whileChatting();		// przesylanie do siebie wiadomosci
			} catch (IOException e) {
				// TODO Auto-generated catch block
				okno_gry.komunikat01.setForeground(Color.red);
				

					okno_gry.komunikat01.setText("POLACZENIE ZERWANE!!!");
					try {
						Thread.sleep(4000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					System.exit(0);
					
				
				
				
				//e.printStackTrace();
			}			// ustala palaczenie i tworzy strumienie
			
			finally
			{	

				closeCrap();		// zamyka pierdoly po zakonczeniu czatu
			}
		}

		private void extracted() {
			stop();
		}
		
		
		
		//******************************  *********************************************
		// get stream to send and receive data   - ustawia strumienie do wysylania i odbierania danych
		private void setupStreams() throws IOException	
		{
			//output pozwala mi na wysylanie wiadomosci
			output = new ObjectOutputStream (connection.getOutputStream());	// ustawia sciezke pozwalajaca wysylac wiadomosci do 2 komputerem
			output.flush();		// splukuje wszystkei dziadostwa do 2 osoby
			
			//input pozwala mi na odbieranie wiadomosci
			input = new ObjectInputStream (connection.getInputStream());	// ustawia sciezki do odbierania wiadomosci od 2 komputera
			
			System.out.println("\n Streams are now setup! \n");
			
			
		}
		
		//******************************  *********************************************
		//podczas konwersacji na chacie (jest juz skonfigurowane polaczenie)
		
		
		
		
		
		
		
		
		private void whileChatting () throws IOException
		{
			String message = "You are now connected!  ";
			sendMessage(message);
			int x, y;

			do
			{
				// konwersacja
				try
				{	
					message = (String) input.readObject();	// przechowuje wiadomosci do nas przychodzace jako obiekty i rzutuje je na string
					System.out.println("\n" + message);			// wyswietla przechwycana wiadomosc na ekranie
					
					
					
					
					
					
					
					
					
					
		// obs�uga odbioru ruchu przeciwnika
					
					
					if (message.matches(".*\\ ATBf1 \\b.*"))	// jesli tekst to ruch gracza to go nie wyswietla w chacie
					{
						
						okno_gry.flagaTura = 0;
						kliknieciePrzeciwnika = message.split(" ");
						
						System.out.println(kliknieciePrzeciwnika[3]);			// 3 -  wspolrzedna x, 4 wspolrzedna y
						
						
						
						x = Integer.parseInt(kliknieciePrzeciwnika[3]);
						y = Integer.parseInt(kliknieciePrzeciwnika[4]);
						
						okno_gry.pole[x][y].setIcon(new ImageIcon(".\\img\\o.jpg"));
						okno_gry.tab[x][y] = 'r';
						okno_gry.pole[x][y].setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
						//okno_gry.sprawdzenie_krzyzyk();

						
						
						
					}
					else
					{	
						appendText(message);
					}
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				}
				catch(ClassNotFoundException classNotFoundException)
				{
					System.out.println("\n I don't know what user send!");	// kiedy uzytkownik wysle cos naprawde dziwnego co sie w stringu nie spasuje to to wywali
				}
				
			}
			while (!message.equals("CLIENT - END"));  // kiedy uzytkownik podczas konwersacji wysle wiadomosc END, to chat zostanie rozlaczony
		
		}
		
		
		
		//******************************  *********************************************
		// zamyka strumienie i sockety po zakonczeniu chatowania
		
		private void closeCrap()
		{
			System.out.println("\n Closing connections... \n");
			try
			{	
				output.close(); 	// zamyka strumien wyjsciowy do gniazda
				input.close();		// zamyka strumien wejsciowy do gniazda
				connection.close(); // zamyka gniazda miedzy 2 komputerami
				
			}
			catch(IOException ioException)
			{
				ioException.printStackTrace();
			}
			
		}
		
		
		

		
	}
	
	
	
	//MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
	
	
	
	//******************************  *********************************************
	//wysyla wiadomosc do klienta		// to nie to samo co showMessage!!
	public void sendMessage(String message)
	{
		try
		{
			output.writeObject("SERVER - " + message); // wysy�a wiadomosc do output - tworzy obiekt i wysy�a go do strumienia wyjsciowego
			output.flush(); 	// jesli by sie okazalo, ze jakies bity nie zostaly przeslane, to ta funkcja je przesle
			System.out.println("\nSERVER - " + message);		// wypisuje ta wiadomosc ktora wysleles w oknie czatu twojego czatu
			
		}
		catch(IOException ioException)	// gdy z jakiegos wzgledu nie uda sie wyslac wiadomosci
		{
			System.out.println("\n ERROR: I CAN'T SEND THAT MESSAGE \n ");
		}
	}
	


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//********************************************************
	
	
	private JFrame jFrame;
	public JPanel jPanel;
	private JTextArea historiaChatu;
	private JTextField poleChatu;
	
	void ChatGui()
	{


		
		jPanel = new JPanel();
		jPanel.setBounds(0, 50, 400, 600);
		//jPanel.setBackground(Color.BLACK);
		jPanel.setLayout(null);

		
		dodajPolaTekstowe();
		
		poleChatu.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed(ActionEvent event)		// jak ktos walnei enter to sie to wykona
				{	
					String WiadomoscDoWyslania = event.getActionCommand();
					sendMessage(WiadomoscDoWyslania);		// wysy�a to co wpisalismy do userTexy
					appendText("Ja: " + WiadomoscDoWyslania);
					poleChatu.setText("");		// po wyslaniu wiadomosci pole tekstowe sie czysci
				}

			}
		);
		
		
	}
	
	//dodaje pole historii chatu i pole do pisania
	void dodajPolaTekstowe()
	{
		historiaChatu = new JTextArea();		
		historiaChatu.setVisible(true);
		jPanel.add(historiaChatu);
		historiaChatu.setBounds(0,1, 300, 300);
		historiaChatu.setEditable(false);

		
		poleChatu = new JTextField();
		poleChatu.setVisible(true);
		jPanel.add(poleChatu);
		poleChatu.setBounds(0,303, 300, 30);
		
		
		
	}
	
	//dodaje tekst ktory zostal przeslany przez chat
	void appendText(String tekst)
	{
		

			historiaChatu.append("Wiadomosc: " + tekst + "\n");

		
	}
	
	
	
	
	//***************************************************************
	
	
	
	
	
	
	
	
}

