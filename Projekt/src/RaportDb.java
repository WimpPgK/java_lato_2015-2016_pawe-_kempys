import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class RaportDb 
{
	private Connection con;	// zmienna pomagaj�ca po��czy� si� z baza danych
	private Statement st;
	private ResultSet rs;	// do tej zmienej pobierane s� zapytania z bazy danych
	private JFrame okno2;
	protected JPanel jPanel;
	protected JPanel panelZnajdzGracza;
	protected JPanel panelNajlepsiGracze;
	protected JPanel panelUzytkownicyOnline;
	private JButton przycisk1;
	private JButton przycisk2;
	private JButton przycisk3;
	private JButton przycisk4;
	private JButton przycisk5;
	private JButton przycisk6;
	private JTextField znajdz;
	private String nazwaUzytkownika ;
	
	private JTextArea jTextArea;
	private JScrollPane jScrollPane;
	
	
	
	public RaportDb()
	{
		
		
		/*    GUI     */
		utworzOkno();
		dodajPrzyciski();
		
		
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver");		// sprawdzamy czy ta klasa mo�e byc znaleziona w naszym projekcie
														// musimy includowa� to do projektu (to co sciaga�em)
			// musimy dodac nowy jar z plikiem mysql connector (to pobrane)
			// prawym na projekt, properties->java build path -> library -> Add external JARs
		
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/statystyki_graczy" , "root", "");// sciezka do bazy danych, nazwa uzytkownika, has�o
			st  = con.createStatement();	// tam nei by�o =
			
		
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex);
		}
		
	}
	
	
	
	

	
	private void utworzOkno()
	{
		
		okno2 = new JFrame ("Statystyki");
		okno2.setSize(750 ,500);
		okno2.setVisible(true);
		okno2.setLocation(430, 200);
		okno2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//okno2.setResizable(false);
		okno2.setLayout(null);
		
		
		
		/*		Panel g��wny		*/
		jPanel = new JPanel();
		jPanel.setSize(750 ,500);
		jPanel.setVisible(true);
		jPanel.setBackground(Color.blue);
		jPanel.setLayout(null);
		okno2.add(jPanel);

		
		
		/*		Panel Znajdz Gracza		*/
		
		
		panelZnajdzGracza = new JPanel();
		panelZnajdzGracza.setSize(750 ,500);
		panelZnajdzGracza.setVisible(true);
		panelZnajdzGracza.setBackground(Color.red);
		panelZnajdzGracza.setLayout(null);
		okno2.add(panelZnajdzGracza);
		panelZnajdzGracza.setVisible(false);

		
		
		
		/*		Panel Najlepsi Gracze		*/
		
		panelNajlepsiGracze = new JPanel();
		panelNajlepsiGracze.setSize(750 ,500);
		panelNajlepsiGracze.setVisible(true);
		panelNajlepsiGracze.setBackground(Color.green);
		panelNajlepsiGracze.setLayout(null);
		okno2.add(panelNajlepsiGracze);
		panelNajlepsiGracze.setVisible(false);
		
		
		/*		Panel Uzytkownicy Online		*/
		
		panelUzytkownicyOnline = new JPanel();
		panelUzytkownicyOnline.setSize(750 ,500);
		panelUzytkownicyOnline.setVisible(true);
		panelUzytkownicyOnline.setBackground(Color.yellow);
		panelUzytkownicyOnline.setLayout(null);
		okno2.add(panelUzytkownicyOnline);
		panelUzytkownicyOnline.setVisible(false);
		
		
		
		
		
		
	}
	
	
	
	void dodajPrzyciski()
	{
		
		przycisk1 = new JButton("Znajd� gracza");
		jPanel.add(przycisk1);
		przycisk2 = new JButton("Najlepsi gracze");
		jPanel.add(przycisk2);
		przycisk3 = new JButton("U�ytkownicy online");
		jPanel.add(przycisk3);
		
		
		przycisk4 = new RaportDb_PrzyciskCofnij().zwroc_przycisk();
		panelZnajdzGracza.add(przycisk4);
		przycisk4.addActionListener(new klikniecie_Cofnij1());
		
		
		przycisk5 = new RaportDb_PrzyciskCofnij().zwroc_przycisk();
		panelNajlepsiGracze.add(przycisk5);
		przycisk5.addActionListener(new klikniecie_Cofnij2());
		
		
		
		przycisk6 = new RaportDb_PrzyciskCofnij().zwroc_przycisk();
		panelUzytkownicyOnline.add(przycisk6);
		przycisk6.addActionListener(new klikniecie_Cofnij3());
		
		
		
		
		
		przycisk1.setBounds(20,20,150,50);
		przycisk1.addActionListener(new klikniecie_ZnajdzGracza());
		
		
		
		przycisk2.setBounds(230,20,150,50);
		przycisk2.addActionListener(new klikniecie_NajlepsiGracze());
		
		
		przycisk3.setBounds(440,20,150,50);
		przycisk3.addActionListener(new klikniecie_UzytkownicyOnline());
		
		
		znajdz = new JTextField();
		jPanel.add(znajdz);
		znajdz.setBounds(20, 80, 150, 50);
		

		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	class klikniecie_ZnajdzGracza implements ActionListener 
	{
		public void actionPerformed (ActionEvent e)
		{
			nazwaUzytkownika = znajdz.getText();
			znajdz.setText("");
			System.out.println(nazwaUzytkownika);
			
			
			
			jPanel.setVisible(false);
			panelZnajdzGracza.setVisible(true);
			przycisk4.setVisible(true);
			
			
			
			
			
			jTextArea = new JTextArea();
			jScrollPane = new JScrollPane(jTextArea);
			jScrollPane.setBounds(10,10,715,350);
			panelZnajdzGracza.add(jScrollPane);
			
			
			Font font = new Font("Verdana", Font.BOLD, 16);
			jTextArea.setFont(font);
			
			try
			{
				Class.forName("com.mysql.jdbc.Driver");		// sprawdzamy czy ta klasa mo�e byc znaleziona w naszym projekcie
															// musimy includowa� to do projektu (to co sciaga�em)
				// musimy dodac nowy jar z plikiem mysql connector (to pobrane)
				// prawym na projekt, properties->java build path -> library -> Add external JARs
			
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/statystyki_graczy" , "root", "");// sciezka do bazy danych, nazwa uzytkownika, has�o
				st  = con.createStatement();	
				
			
			}
			catch(Exception ex)
			{
				System.out.println("Error: " + ex);
			}
			
			
			
			
			
			
			try
			{
				String query = "select persons.nick, persons.RokUrodzenia, persons.liczba_wygranych, persons.liczba_przegranych, gracze_online.status from persons, gracze_online where persons.id = gracze_online.id and persons.nick = '" + nazwaUzytkownika + "'" ;	// tworzymy zapytanie (zazpytanie w sql kt�re ma sie wykonac
				rs = st.executeQuery(query);			// executeQu bery = eng: powtarzanie zapytania
				System.out.println("Records from database");

				//System.out.println(znajdz);	
				
				jTextArea.append("   Wygrane       Przegrane       Rok Urodzenia       Nick           Status\n\n");
				
				while (rs.next())						// wypisujemy wszystkie rekordy z bazy danych
				{	
					
	 
					
					String nick = rs.getString("nick"); // pobiera dane z bazy danych kolumna o nazwie name
					//String miejscowosc = rs.getString("miejscowosc");
					String wiek = rs.getString("RokUrodzenia");
					String liczba_wygranych = rs.getString("liczba_wygranych");
					String liczba_przegranych = rs.getString("liczba_przegranych");
					String status = rs.getString("status");
						jTextArea.append("        " + liczba_wygranych + "                     " + liczba_przegranych + "                    " + wiek + "                    " + nick + "          " + status + "\n");

						
					
				}
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
			
			
			
			
			
			
			
			
			
			
			

		}
	}
	
	
	class klikniecie_NajlepsiGracze implements ActionListener 
	{
		public void actionPerformed (ActionEvent e)
		{
			jPanel.setVisible(false);
			panelNajlepsiGracze.setVisible(true);
			przycisk5.setVisible(true);
			
			
			
			jTextArea = new JTextArea();
			jScrollPane = new JScrollPane(jTextArea);
			jScrollPane.setBounds(10,10,715,350);
			panelNajlepsiGracze.add(jScrollPane);
			
			
			Font font = new Font("Verdana", Font.BOLD, 16);
			jTextArea.setFont(font);
			
			try
			{
				Class.forName("com.mysql.jdbc.Driver");		// sprawdzamy czy ta klasa mo�e byc znaleziona w naszym projekcie
															// musimy includowa� to do projektu (to co sciaga�em)
				// musimy dodac nowy jar z plikiem mysql connector (to pobrane)
				// prawym na projekt, properties->java build path -> library -> Add external JARs
			
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/statystyki_graczy" , "root", "");// sciezka do bazy danych, nazwa uzytkownika, has�o
				st  = con.createStatement();	
				
			
			}
			catch(Exception ex)
			{
				System.out.println("Error: " + ex);
			}
			
			
			
			
			
			
			try
			{
				String query = "select persons.nick, persons.RokUrodzenia, persons.liczba_wygranych, persons.liczba_przegranych, gracze_online.status from persons, gracze_online where persons.id = gracze_online.id ORDER by persons.liczba_wygranych desc";	// tworzymy zapytanie (zazpytanie w sql kt�re ma sie wykonac
				rs = st.executeQuery(query);			// executeQu bery = eng: powtarzanie zapytania
				System.out.println("Records from database");
				int i = 1;
						
				
				jTextArea.append(" Ranking       Wygrane       Przegrane       Rok Urodzenia       Nick           Status\n\n");
				
				while (rs.next() && i < 15)						// wypisujemy wszystkie rekordy z bazy danych
				{	
					
	 
					
					String nick = rs.getString("nick"); // pobiera dane z bazy danych kolumna o nazwie name
					//String miejscowosc = rs.getString("miejscowosc");
					String wiek = rs.getString("RokUrodzenia");
					String liczba_wygranych = rs.getString("liczba_wygranych");
					String liczba_przegranych = rs.getString("liczba_przegranych");
					String status = rs.getString("status");
					if(i < 10)
					{
						jTextArea.append("       "+ i +  "                    " + liczba_wygranych + "                     " + liczba_przegranych + "                    " + wiek + "                    " + nick + "            " + status + "\n");
					}
					else
					{
						jTextArea.append("      "+ i +  "                   " + liczba_wygranych + "                     " + liczba_przegranych + "                    " + wiek + "                    " + nick + "            " + status + "\n");
					}
					
					 
					i++;
					
				}
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
			
			
			
			
			
			
			
			
		}
	}
	
	
	
	
	
	
	
	
	
	//********************************************************************************
	
	class klikniecie_UzytkownicyOnline implements ActionListener 
	{
		public void actionPerformed (ActionEvent e)
		{
			
			jPanel.setVisible(false);
			panelUzytkownicyOnline.setVisible(true);
			przycisk6.setVisible(true);
			
			
			jTextArea = new JTextArea();
			jScrollPane = new JScrollPane(jTextArea);
			jScrollPane.setBounds(10,10,715,350);
			panelUzytkownicyOnline.add(jScrollPane);
			
			
			Font font = new Font("Verdana", Font.BOLD, 20);
			jTextArea.setFont(font);
			
			
			
			try
			{
				Class.forName("com.mysql.jdbc.Driver");		// sprawdzamy czy ta klasa mo�e byc znaleziona w naszym projekcie
															// musimy includowa� to do projektu (to co sciaga�em)
				// musimy dodac nowy jar z plikiem mysql connector (to pobrane)
				// prawym na projekt, properties->java build path -> library -> Add external JARs
			
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/statystyki_graczy" , "root", "");// sciezka do bazy danych, nazwa uzytkownika, has�o
				st  = con.createStatement();	
				
			
			}
			catch(Exception ex)
			{
				System.out.println("Error: " + ex);
			}
			
			
			
			
			
			
			try
			{
				String query = "select persons.nick, persons.liczba_wygranych, persons.liczba_przegranych, gracze_online.status from persons, gracze_online where persons.id = gracze_online.id and status = 'online'";	// tworzymy zapytanie (zazpytanie w sql kt�re ma sie wykonac
				rs = st.executeQuery(query);			// executeQu bery = eng: powtarzanie zapytania
				System.out.println("Records from database");
			
						
				
				jTextArea.append("      Wygrane                 Przegrane                 Nick \n\n");
				
				while (rs.next())						// wypisujemy wszystkie rekordy z bazy danych
				{	
					
	 
					
					String nick = rs.getString("nick"); // pobiera dane z bazy danych kolumna o nazwie name
					//String miejscowosc = rs.getString("miejscowosc");
					//String wiek = rs.getString("RokUrodzenia");
					String liczba_wygranych = rs.getString("liczba_wygranych");
					String liczba_przegranych = rs.getString("liczba_przegranych");
					String status = rs.getString("status");
				
						jTextArea.append("            " + liczba_wygranych + "                               " + liczba_przegranych +"                       " + nick +" \n");
				
					 
			
					
				}
			}
			catch(Exception ex)
			{
				System.out.println(ex);
			}
			
			
			
			
			
			
			
			
		}
	}
	
	
	
	
	
	
	//*****************************   PRZYCISKI COFNIJ ******************************************

	class klikniecie_Cofnij1 implements ActionListener  
	{
		public void actionPerformed (ActionEvent e)
		{
			
			
			panelZnajdzGracza.setVisible(false);
			jPanel.setVisible(true);
			przycisk4.setVisible(false);
			
		}
	}
	
	
	
	class klikniecie_Cofnij2 implements ActionListener  
	{
		public void actionPerformed (ActionEvent e)
		{
			
			
			panelNajlepsiGracze.setVisible(false);
			jPanel.setVisible(true);
			przycisk5.setVisible(false);
		}
	}
	
	
	
	class klikniecie_Cofnij3 implements ActionListener  
	{
		public void actionPerformed (ActionEvent e)
		{
			
			
			panelUzytkownicyOnline.setVisible(false);
			jPanel.setVisible(true);
			przycisk6.setVisible(false);
		}
	}
	
	//*****************************   KONIEC  ******************************************
	
	
	
	
	
	
	
	public void getData()	// pobiera rekordy z bazy i je wypisuje
	{
		
		
		/*
		
		try
		{
			String query = "select * from persons";	// tworzymy zapytanie (zazpytanie w sql kt�re ma sie wykonac
			rs = st.executeQuery(query);			// executeQu bery = eng: powtarzanie zapytania
			System.out.println("Records from database");
			
			
			

			
			JLabel [] wiersz1 = new JLabel[30];
			JLabel [] wiersz2 = new JLabel[30];
			JLabel [] wiersz3 = new JLabel[30];
			JLabel [] wiersz4 = new JLabel[30];
			JLabel [] wiersz5 = new JLabel[30];
			
			int i = 2;
			wiersz1[1] = new JLabel("Nick");
			wiersz1[1].setBounds(40, 20, 90, 30);
			wiersz1[1].setVisible(true);
			okno2.add(wiersz1[1]);
			
			
			wiersz2[1] = new JLabel("Miejscowosc");
			wiersz2[1].setBounds(160, 20, 190, 30);
			wiersz2[1].setVisible(true);
			okno2.add(wiersz2[1]);
			
			wiersz3[1] = new JLabel("Wiek");
			wiersz3[1].setBounds(340, 20, 90, 30);
			wiersz3[1].setVisible(true);
			wiersz3[1].setVisible(true);
			okno2.add(wiersz3[1]);
			
			wiersz4[1] = new JLabel("Liczba wygranych");
			wiersz4[1].setBounds(440, 20, 150, 30);
			wiersz4[1].setVisible(true);
			okno2.add(wiersz4[1]);
			wiersz4[1].setVisible(true);
			
			wiersz5[1] = new JLabel("Liczba przegranych");
			wiersz5[1].setBounds(600, 20, 190, 30);
			wiersz5[1].setVisible(true);
			okno2.add(wiersz5[1]);
			wiersz5[1].setVisible(true);
			
			while (rs.next() && i < 31)						// wypisujemy wszystkie rekordy z bazy danych
			{	
				
 
				
				String nick = rs.getString("nick"); // pobiera dane z bazy danych kolumna o nazwie name
				String miejscowosc = rs.getString("miejscowosc");
				String wiek = rs.getString("wiek");
				String liczba_wygranych = rs.getString("liczba_wygranych");
				String liczba_przegranych = rs.getString("liczba_przegranych");
				
				
				wiersz1[i] = new JLabel(nick);
				wiersz1[i].setBounds(30, i*30, 90, 30);
				wiersz1[i].setVisible(true);
				okno2.add(wiersz1[i]);
				
				
				wiersz2[i] = new JLabel(miejscowosc);
				wiersz2[i].setBounds(160, i*30, 190, 30);
				wiersz2[i].setVisible(true);
				okno2.add(wiersz2[i]);
				
				wiersz3[i] = new JLabel(wiek);
				wiersz3[i].setBounds(350, i*30, 90, 30);
				wiersz3[i].setVisible(true);
				wiersz3[i].setVisible(true);
				okno2.add(wiersz3[i]);
				
				wiersz4[i] = new JLabel(liczba_wygranych);
				wiersz4[i].setBounds(470, i*30, 150, 30);
				wiersz4[i].setVisible(true);
				okno2.add(wiersz4[i]);
				wiersz4[i].setVisible(true);
				
				wiersz5[i] = new JLabel(liczba_przegranych);
				wiersz5[i].setBounds(630, i*30, 190, 30);
				wiersz5[i].setVisible(true);
				okno2.add(wiersz5[i]);
				wiersz5[i].setVisible(true);
				
				i++;
				
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		*/
	}
	
	
	
}