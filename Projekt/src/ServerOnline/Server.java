import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server 
{
	public final int PORT = 5554;
	
	
	//******************************  MAIN  *********************************
	public static void main(String[] args) throws IOException
	{
		
		//Server s1 = new Server();		// wtedy nie musial main wyrzucac wyjatku tylko objac s1.run...
		//s1.runServer();
		
		
		new Server().runServer ();


	}
	
	
	
	
	//**********************************************************************
	public void runServer () throws IOException
	{
		ServerSocket serverSocket = new ServerSocket (PORT);
		System.out.println("Serwer jest gotowy i czeka na polaczenie...");
		
		while(true)
		{
			Socket socket = serverSocket.accept();
			
			new ServerThread(socket).start();	// wywo�anie start powoduje uruchomienei metody run
			System.out.println("OU YEAH! Odpalono nowy watek. ");
		
		}
		
		
		
	}

}
