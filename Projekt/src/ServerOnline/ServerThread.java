import java.io.*;

import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ServerThread extends Thread 
{
	//SOCKET//
	Socket socket;
	String adresIP;
	
	int idGracza;		// przechowije id gracza ktore jest przeslane podczas pierwszego logowania
	
	
	
	//DB//
	private GraczOnlineDB graczOnlineDB;
	

	
	ServerThread(Socket socket)
	{
		this.socket = socket;
		//String adresIP = socket.getRemoteSocketAddress().toString();	127.0.0.1:50117 np.
		adresIP = socket.getInetAddress().toString();  ///  getInetAddress();
		
		// to ustawiania statusu gracza online/offline
		graczOnlineDB = new GraczOnlineDB(adresIP);
		
		
		System.out.println(adresIP);
	}
	
	public void run()
	{
		try 
		{	
			String message = null;
			
			// pozwala na wys�anie tekstu do strumeinia wyjscia (pobieramy tekst kt�ry dostalismy od uzytkownika bo romiby echo)
			//PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
			
			
			
			// bufferReader pobiera to co jest przysy�ane w InputStream, czyli wiadomosci od klienta
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			
			
			
	//		idGracza 
			String id_gracza = bufferedReader.readLine();
			System.out.println("ID string GRACZA WYNOSI " + id_gracza);
			idGracza = Integer.valueOf(id_gracza);
			
			System.out.println("U�ytkownik o id:      " + id_gracza + "      jest teraz polaczony z serwerem...");
			
			
			
			
			// teraz gdy zamy id gracza to wrzucamy go do bazy danych
			graczOnlineDB.NowyGraczOnline(idGracza);	// ustawia status gracza na online i zapamietuje jego ip
			
			while ((message = bufferedReader.readLine()) != null)
			{
				System.out.println("Wiadomosc przychodzaca: " + message);
				
				// pozwala na wys�anie tekstu do strumeinia wyjscia
				//printWriter.println("Server powtarza wiadomosc klienta ==> " + message);	
			}
			socket.close();
		
		}
		catch(IOException e)// gdy stracimy poloczenie z klientem wyrzuca ten blad, bo wiadomo�� jest null
		{
			System.out.println("Uzytkownik polegl na polu chwaly! " + "  RIP  " + adresIP );
			graczOnlineDB.graczOffline();
			
		}
	}
	
}


