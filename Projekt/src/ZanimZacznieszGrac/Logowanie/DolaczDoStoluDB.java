package ZanimZacznieszGrac.Logowanie;


import java.sql.Connection;
import java.sql.Statement;

import ZanimZacznieszGrac.KonfiguracjaPolaczeniaDB;

public class DolaczDoStoluDB 
{
	Statement st;
	Connection con;
	int idStolu;
	int idGracza;
	
	public DolaczDoStoluDB() 
	{
		try
		{

			KonfiguracjaPolaczeniaDB k1 = new KonfiguracjaPolaczeniaDB();
			st = k1.ZwrocSt();
			con = k1.ZwrocCon();
		
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex);
		}
	}
	
	
	//***********************************************************************
	public void gracz1DB(int idStolu, int idGracza) throws Exception
	{
		
		this.idStolu = idStolu;
		this.idGracza = idGracza;
		String query= "UPDATE `stoly` SET `ID_graczSerwer` = "+ idGracza + " WHERE `stoly`.`ID_stolu` =" + idStolu;
		st.executeUpdate(query);			// executeQuery = eng: powtarzanie zapytania
	}
	
	
	
	//***********************************************************************
	public void gracz2DB(int idStolu, int idGracza) throws Exception
	{
		this.idStolu = idStolu;
		this.idGracza = idGracza;
		String query = "UPDATE `stoly` SET `ID_graczKlient` = "+ idGracza + " WHERE `stoly`.`ID_stolu` =" + idStolu;
		st.executeUpdate(query);
	}
	
	
	

}
