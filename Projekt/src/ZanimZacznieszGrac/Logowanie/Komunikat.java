package ZanimZacznieszGrac.Logowanie;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;

// s�u�y do wypisywania komunikat�w w nowym oknie, jako argument przyjmuje komunikat
public class Komunikat 
{
	
	
	private JPanel jPanelKomunikat;
	private JFrame jFrameKomunikat;
	private JTextArea jTextArea;
	
	
	
	String napis;
	
	public Komunikat()
	{
		
		utworzOknoKomunikatu();

		
		
	}
	
	// tworzy okno do wyswietlania komunikatow
	private void utworzOknoKomunikatu()
	{
		jFrameKomunikat = new JFrame ("New user");
		jFrameKomunikat.setBounds(200, 200, 300, 200);
		jFrameKomunikat.setLayout(null);
		jFrameKomunikat.setVisible(false);
		jFrameKomunikat.setAlwaysOnTop(true);
		
				
		jPanelKomunikat = new JPanel();
		jPanelKomunikat.setSize(300, 200);
		jPanelKomunikat.setLayout(null);
		//jPanelKomunikat.setBackground(Color.BLACK);
		

		jFrameKomunikat.add(jPanelKomunikat);
		
	}
	
	public void wypisz (String napis)
	{
		this.napis = napis;
		dodajKomunikat();
		jFrameKomunikat.setVisible(true);
		
	}
	private void dodajKomunikat()
	{
		
		jTextArea = new JTextArea();
		//jTextArea.setMinimumSize(new Dimension(0,0));
		jTextArea.setText(napis);
		jTextArea.setVisible(true);
		jTextArea.setEditable(false);
		jTextArea.setBounds(0,0, 300, 200);
		//jTextArea.setBackground(Color.lightGray);
		jPanelKomunikat.add(jTextArea);
	}
}
