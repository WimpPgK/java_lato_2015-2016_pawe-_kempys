package ZanimZacznieszGrac.Rejestracja;

import java.sql.*;
import java.sql.ResultSet;

import ZanimZacznieszGrac.KonfiguracjaPolaczeniaDB;

public class DbAdd
{
	private Connection con;	// zmienna pomagaj�ca po��czy� si� z baza danych
	private Statement st;
	private ResultSet rs;
	private String login;		// musimy to zczyta� z gui
	private String password;
	private String nick;
	private String miejscowosc;
	private int rok_urodzenia;
	private int id;
	public int flaga; // 3 jesli wszystko ok, 2 jesli zly nick, 1 jesli z�y login, 0 gdy z�y nick i login
	private String query;
	
	
	public DbAdd(String log, String passwd, String ni, String miejsc, int rok)	// login, has�o, nazwa uzytkownika, miejscowosc, data urodzenia
	{	
		flaga = 0;
		login = log;
		password = passwd;
		nick = ni;
		miejscowosc = miejsc;
		rok_urodzenia = rok;
		System.out.println("Dzialam");
		
		try
		{

			KonfiguracjaPolaczeniaDB k1 = new KonfiguracjaPolaczeniaDB();
			st = k1.ZwrocSt();
			con = k1.ZwrocCon();
		
		}
		catch(Exception ex)
		{
			System.out.println("Error: " + ex);
		}
		
	}
	
	public void checkData()	// pobiera rekordy z bazy i je wypisuje
	{

			//************************
			try	// sprawdzamy czy istneje gracz o wpisanym nicku (jesli tak to musimy utworzyc innego urzytkownika)
			{
				query =  "SELECT nick from persons where nick = '"+ nick + "'";
				
				
				rs = st.executeQuery(query);			// executeQuery = eng: powtarzanie zapytania
				System.out.println("Records from database");

				rs.next();
				String odczyt = rs.getString("nick"); // w zmiennej odczyt przechowywane jest haslo zczytane z bazy danych
				System.out.println(odczyt);
				System.out.println("Podana nazwa uzytkownika jest ZAJETA");
				
			}
			catch(Exception ex)// jak wyrzuci wyjatek to znaczy ze taki gracz jeszcze nie istnieje wiec mozemy go utworzyc
			{
				System.out.println("Podana nazwa uzytkownika jest wolna");
				flaga += 2;
			}
			
			
			//************************
			try	// sprawdzamy czy istneje gracz o wpisanym loginie (jesli tak to musimy utworzyc innego urzytkownika)
			{
				query =  "SELECT login from hasla where login = '"+ login + "'";
				
				
				rs = st.executeQuery(query);			// executeQuery = eng: powtarzanie zapytania
				System.out.println("Records from database");

				rs.next();
				String odczyt = rs.getString("login"); // w zmiennej odczyt przechowywane jest haslo zczytane z bazy danych
				System.out.println(odczyt);
				System.out.println("Podana login jest ZAJETY!!!");
				
			}	// jak wyrzuci wyjatek to znaczy ze taki gracz jeszcze nie istnieje wiec mozemy go utworzyc
			catch(Exception ex)
			{
				System.out.println("Podana login jest wolny");
				flaga ++;
			}

	}
	
	// jezeli juz wiemy ze dany uzytkownik nie istnieje to tworzymy go w bazie danych
	public void insertData()
	{
		try
		{
			
			query = "INSERT INTO `persons` (`id`, `nick`, `miejscowosc`, `RokUrodzenia`, `liczba_wygranych`, `liczba_przegranych`) VALUES (NULL, '" + nick + "' , '"+ miejscowosc + "' , '"+ rok_urodzenia + "', '0', '0')";
			st.executeUpdate(query);			// executeQuery = eng: powtarzanie zapytania
			
			
			
			try
			{
				// pobieramy sobie id, �eby wstawic odpowiednie id do bazy danych
				query = "SELECT id from persons where nick = '"+ nick + "'";
				rs = st.executeQuery(query);
				rs.next();
				id = rs.getInt("id");
				System.out.println(id);
				
				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}


			// dodajemy ha�o i login do naszej bazy
			query = "INSERT INTO `hasla` (`id`, `login`, `password`) VALUES (" + id + " , '" + login + "' , '" + password + "')"; 
			st.executeUpdate(query); 
			query = "INSERT INTO `typ_konta_gracza` (`id`, `ID_rodzajKonta`) VALUES ('" + id + "', '4')";
			st.executeUpdate(query); 
			query = "INSERT INTO `gracze_online` (`id`, `status`, `adresIP`) VALUES ('"+ id + "', 'offline', 'brak')";
			st.executeUpdate(query);
		
		}
		catch(Exception ex)
		{
			System.out.println("Cos zle");
		}
	}
	
	
}
	