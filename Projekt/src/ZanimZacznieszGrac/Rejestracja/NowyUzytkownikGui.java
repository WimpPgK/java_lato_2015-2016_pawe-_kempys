package ZanimZacznieszGrac.Rejestracja;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import javax.swing.JFrame;

import ZanimZacznieszGrac.Logowanie.Komunikat;



public class NowyUzytkownikGui 
{
	private JFrame jFrame;
	private JPanel jPanel;
	private Komunikat komunikat;

	
	private JButton cofnij;
	private JButton wyczysc;
	private JButton utworzUzyrkownika;
	
	
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JLabel label5;
	private JLabel label6;

	
	private JTextField nazwaUzytkownika;
	
	private JTextField login;
	private JPasswordField password;
	private JPasswordField password2;
	private JTextField miejscowosc;
	private JTextField dataUrodzenia;
	
	
	public int wysokosc;
	public int szerokosc;
	
	public NowyUzytkownikGui()
	{	
		
		komunikat = new Komunikat(); // tworzymy obiekt do wyrzucania bledu
		szerokosc = 400;
		wysokosc = 450;
		
		jFrame = new JFrame ("New user");
		jFrame.setBounds(150, 150, 400,450);
		jFrame.setLayout(null);
		jFrame.setVisible(true);
		//jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
				
		jPanel = new JPanel();
		jPanel.setSize(szerokosc, wysokosc);
		//jPanel.setBackground(Color.BLACK);
		jPanel.setLayout(null);

		jFrame.add(jPanel);
		
		addCraps();	// dodaje wszystkie elementy do jPanelu
				
		
	}
	
	
	

	
	
	// Ustawia wszystke elementy panelu logowania. Dodaje je do jPanelu
	private void addCraps()
	{
		
	// NAPISY	
		label1 = new JLabel ("Nazwa u�ytkownika             (4-12)");
		label1.setBounds(20, 30, 190, 40 );
		label1.setVisible(true);
		label1.setOpaque(true);
		//label1.setBackground(Color.RED);
		jPanel.add(label1);
		
		label2 = new JLabel ("Login                                        (4-12)");
		label2.setBounds(20, 70, 190, 40);
		label2.setVisible(true);
		label2.setOpaque(true);
		//label2.setBackground(Color.RED);
		jPanel.add(label2);
		
		label3 = new JLabel ("Has�o                                       (4-12)");
		label3.setBounds(20, 110, 190, 40);
		label3.setVisible(true);
		label3.setOpaque(true);
		//label3.setBackground(Color.RED);
		jPanel.add(label3);
		
		label4 = new JLabel ("Powt�rz has�o                       (4-12)");
		label4.setBounds(20, 150, 190, 40);
		label4.setVisible(true);
		label4.setOpaque(true);
		//label4.setBackground(Color.RED);
		jPanel.add(label4);
		
		label5 = new JLabel ("Miejscowo��                         (2-20)");
		label5.setBounds(20, 190, 190, 40);
		label5.setVisible(true);
		label5.setOpaque(true);
		//label5.setBackground(Color.RED);
		jPanel.add(label5);
		
		label6 = new JLabel ("Rok urodzenia:  ");
		label6.setBounds(20, 230, 190, 40);
		label6.setVisible(true);
		label6.setOpaque(true);
		//label6.setBackground(Color.RED);
		jPanel.add(label6);
	
		
		
		
		
	// PANELE TEKSTOWE 
		nazwaUzytkownika = new JTextField();
		jPanel.add(nazwaUzytkownika);
		nazwaUzytkownika.setBounds(230, 30, 130, 40 );
		nazwaUzytkownika.setVisible(true);
		nazwaUzytkownika.setOpaque(true);
		//nazwaUzytkownika.setBackground(Color.RED);
		
		
		login = new JTextField();
		jPanel.add(login);
		login.setBounds(230, 70, 130, 40 );
		login.setVisible(true);
		login.setOpaque(true);
		//login.setBackground(Color.RED);
		
		
		password = new JPasswordField();
		jPanel.add(password, Integer.valueOf(1));
		password.setBounds(230, 110, 130, 40  );
		password.setVisible(true);
		password.setOpaque(true);
		//password.setBackground(Color.green);
		
		
		password2 = new JPasswordField();
		jPanel.add(password2, Integer.valueOf(1));
		password2.setBounds(230, 150, 130, 40  );
		password2.setVisible(true);
		password2.setOpaque(true);
		//password2.setBackground(Color.green);
		
		miejscowosc = new JTextField();
		jPanel.add(miejscowosc);
		miejscowosc.setBounds(230, 190, 130, 40 );
		miejscowosc.setVisible(true);
		miejscowosc.setOpaque(true);
		//miejscowosc.setBackground(Color.RED);
		
		dataUrodzenia = new JTextField();
		jPanel.add(dataUrodzenia);
		dataUrodzenia.setBounds(230, 230, 130, 40 );
		dataUrodzenia.setVisible(true);
		dataUrodzenia.setOpaque(true);
		//dataUrodzenia.setBackground(Color.RED);

		
		
	// PRZYCISKI
		/*
		private JButton cofnij;
		private JButton wyczysc;
		private JButton utworzUzyrkownika; 
		*/
		
		cofnij = new JButton("Cofnij");
		jPanel.add(cofnij);
		cofnij.setBounds(20, 300, 100, 30 );
		cofnij.setVisible(true);
		//cofnij.addActionListener(new klikniecieCofnij());
		
		wyczysc = new JButton("Wyczy��");
		jPanel.add(wyczysc);
		wyczysc.setBounds(260, 300, 100, 30 );
		wyczysc.setVisible(true);
		wyczysc.addActionListener(new klikniecie_Wyczysc());
		
		utworzUzyrkownika = new JButton("Utw�rz u�ytkownika");
		jPanel.add(utworzUzyrkownika);
		utworzUzyrkownika.setBounds(20, 340, 340, 50 );
		utworzUzyrkownika.setVisible(true);
		utworzUzyrkownika.addActionListener(new klikniecie_UtworzUzyrkownika());

		
	}
	


	
	
	
// ******************************************************************************************
	class klikniecie_Wyczysc implements ActionListener // czyszczenie wpisanego tekstu
	{

		public void actionPerformed (ActionEvent e)
		{	
			
			
			nazwaUzytkownika.setText("");
			login.setText("");
			password.setText("");
			password2.setText("");
			miejscowosc.setText("");
			dataUrodzenia.setText("");
		}
	
	}
	
	
	
// ******************************************************************************************	
	// obs�uga klikniecia przycisku zaloguj sie
	// pobiera dane z pol tekstowych i wysy�a je go klasy laczacej z baza danych
	class klikniecie_UtworzUzyrkownika implements ActionListener 

	{
		boolean flaga = false;		// gdy flaga bedzie true to wypisze komunikat o blednie wpisanych danych
		StringBuffer tekstBuforowany = new StringBuffer("                Wpisane dane s� niepoprawne.\n");  // ten napis zostanie wyswietlony.
			
		String ro;
		
		
		
		String nazwaUz;		// przechowuje nazwe uzytkownika
		String log;			// przechowuje login
		String pas;			// przechowuje has�o
		String miejsc;		// przechowuje nazwe miejscowosci
		int foo;			// przechowuje rok urodzenia
		
		public void actionPerformed (ActionEvent e)
		{	
			
			flaga = false;
			System.out.println(wysokosc);
			
			try
			{
				
				nazwaUz = nazwaUzytkownika.getText();		// pobieramy nazwe uzytkownika z pola tekstowego
				System.out.println(nazwaUz.length());
				if (nazwaUz.length() > 12 ||  nazwaUz.length() < 4)
				{
					flaga = true;
					tekstBuforowany.append("\n  Nazwa uzytkownika musi zawierac 4-12 znak�w.");
					nazwaUzytkownika.setBackground(Color.RED);
					nazwaUzytkownika.setText("");
					
				}
				else
				{
					nazwaUzytkownika.setBackground(Color.GREEN);
				}
				
				log = login.getText();
				if (log.length() > 12 ||  log.length() < 4)
				{
					flaga = true;
					tekstBuforowany.append("\n  Login musi zawierac 4-12 znak�w.");
					login.setBackground(Color.RED);
					login.setText("");
				}
				else
				{
					login.setBackground(Color.GREEN);
				}
				
				pas = String.valueOf(password.getPassword());	// pobieramy haslo z pola tekstowego
				String pas2 = String.valueOf(password2.getPassword());
				
				if (pas.length() > 12 ||  pas.length() < 4)
				{
					flaga = true;
					tekstBuforowany.append("\n  Has�o musi zawierac 4-12 znak�w.");
					password.setBackground(Color.RED);
					password2.setBackground(Color.RED);
					password.setText("");
					password2.setText("");

				}

				else if (!pas.equals(pas2))
				{
					flaga = true;
					tekstBuforowany.append("\n  Wpisz dwa razy identyczne has�o.");
					password.setBackground(Color.RED);
					password2.setBackground(Color.RED);
					password.setText("");
					password2.setText("");
				}
				else
				{
					password.setBackground(Color.GREEN);
					password2.setBackground(Color.GREEN);
				}

				
				
				miejsc = miejscowosc.getText();
				if (miejsc.length() > 20 ||  miejsc.length() < 2)
				{
					flaga = true;
					tekstBuforowany.append("\n  Nazwa miejscowo�ci musi zawierac 2-20 znak�w.");
					miejscowosc.setBackground(Color.RED);
					miejscowosc.setText("");
				}
				else
				{
					miejscowosc.setBackground(Color.GREEN);
				}
				
				ro = dataUrodzenia.getText();
				
				
				// Sprawdzamy, czy zosta�a wpisana poprawny rok
				try 
				{
					
				      foo = Integer.parseInt(ro);		// rzutowanie roku urodzenia ze string na int
				      
				     
				      if ((foo < 1900) || (foo > 2013))	// sprawdzamy czy wartosc roku sie zgadza
				      {		
				    	  	tekstBuforowany.append("Rok urodzenia musi by� z przedzia�u 1900-2013");
							flaga = true;
							dataUrodzenia.setBackground(Color.RED);
							dataUrodzenia.setText("");
				      }
				      else
				      {
				    	  dataUrodzenia.setBackground(Color.GREEN);
				      }
				      
				} 
				catch (NumberFormatException a) // jesli rok jest zle wpisany 
				{
					
					tekstBuforowany.append("\n  Niepoprawne znaki w roku urodzenia.");
					flaga = true;
					dataUrodzenia.setBackground(Color.RED);
					dataUrodzenia.setText("");
				}
				

				
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
			if (flaga == true)
			{	
				
				komunikat.wypisz("\n\n\n\n                 Wpisane dane s� niepoprawne.");
				//komunikat.wypisz(tekstBuforowany.toString());		// zamieniamy stringa buforowanego do stringa
			}
			else
			{
				System.out.println("BAZA NIEPODLACZONA");
				DbAdd user = new DbAdd (log, pas, nazwaUz , miejsc, foo);	// login, has�o, nazwa uzytkownika, miejscowosc, data urodzenia
				user.checkData();
				
				if (user.flaga == 3)	// oznacza to ze nazwa uzytkownika i login jest wolny
				{
					user.insertData();
					komunikat.wypisz("Rejestracja zako�czy�a si� pomy�lnie");
				}
				else if (user.flaga == 2)	// login jest zajety
				{
					komunikat.wypisz("Podany nick jest ju� zajety");
					flaga = true;
					login.setBackground(Color.RED);

				}
				else if (user.flaga == 1)
				{
					komunikat.wypisz("Podany nick jest ju� zaj�ty");	// nick jest zajete
					flaga = true;
					nazwaUzytkownika.setBackground(Color.RED);
				}
				
			}

		}
	}
	
	

}
